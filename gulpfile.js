const { src, dest, watch, parallel } = require("gulp");

//COMPILAR SASS A CSS
const sass = require("gulp-sass")(require("sass"));
const autoprefixer = require("autoprefixer");
const cssnano = require("cssnano");
const postcss = require("gulp-postcss");
const sourcemaps = require ("gulp-sourcemaps");


function compilandoSaSS(done){
    src("src/scss/**/*.scss")
       .pipe(sourcemaps.init())
       .pipe(sass())
       .pipe(postcss([ autoprefixer(), cssnano()]))  
       .pipe(sourcemaps.write("."))
       .pipe(dest("build/css"))
    done();
}

// javascript
const terser = require("gulp-terser-js");
function javascript(done) {
    src("src/js/**/*.js")
       .pipe(sourcemaps.init())
       .pipe(terser())
       .pipe(sourcemaps.write("."))
       .pipe(dest("build/js"));
    done();
}


function dev(done){
    watch("src/scss/**/*.scss", compilandoSaSS);
    watch("src/js/**/*.js", javascript);
    done();
}

//IMAGENES
const webp = require("gulp-webp");

function versionWebp(done){
    const opciones = {
        quality: 50
    };
    src("src/img/**/*.{jpg,png}")
       .pipe(webp(opciones))  
       .pipe(dest("build/img"))

    done();
}


const imagemin = require("gulp-imagemin");
const cache = require ("gulp-cache");

function imagenes(done){
    const opciones = {
        optimizationLevel: 3
    };
    src("src/img/**/*.{jpg,png}")
       .pipe(cache(imagemin(opciones)))  
       .pipe(dest("build/img"))
    done();
}


const avif = require("gulp-avif")
function versionAvif(done){
    const opciones = {
        quality: 50
    };
    src("src/img/**/*.{jpg,png}")
       .pipe(avif(opciones))  
       .pipe(dest("build/img"))
    done();
}



exports.compilandoSaSS = compilandoSaSS;
exports.js = javascript;
exports.imagenes = imagenes;
exports.versionWebp = versionWebp;
exports.versionAvif = versionAvif;
exports.dev = parallel(imagenes, versionWebp, versionAvif, javascript, dev);



